
var inject = function (w, o) {
  let nullify = function (e) {
    try {
      const methods = Object.getOwnPropertyNames(e.prototype);
      for (let i = 0; i < methods.length; i++) {
        try {
          Object.defineProperty(e.__proto__,  methods[i], {"value": function () {return null}});
          Object.defineProperty(e.prototype,  methods[i], {"value": function () {return null}});
        } catch (e) {}
      }
    } catch (e) {}
  };
  //
  for (let i in o) {
    if (o[i]) {
      if (w === 1) nullify(window[i]);
      else {
        const iframes = window.top.document.querySelectorAll("iframe[sandbox]");
        for (var c = 0; c < iframes.length; c++) {
          if (iframes[c].contentWindow) {
            nullify(iframes[c].contentWindow[i]);
          }
        }
      }
    }
  }
  //
  document.documentElement.dataset.webapiscriptallow = true;
};

var script_1 = document.createElement('script');
script_1.textContent = "(" + inject + ")(1, " + JSON.stringify(options) + ")";
document.documentElement.appendChild(script_1);

if (document.documentElement.dataset.webapiscriptallow !== "true") {
  var script_2 = document.createElement('script');
  script_2.textContent = "(" + inject + ")(2, " + JSON.stringify(options) + ")";
  window.top.document.documentElement.appendChild(script_2);
}
