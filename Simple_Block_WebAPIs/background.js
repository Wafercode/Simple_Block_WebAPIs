
var app = {};

app.tab = {
  "open": function (url) {chrome.tabs.create({"url": url, "active": true})},
  "reload": function (id, callback) {chrome.tabs.reload(id, {"bypassCache": true}, callback)},
  "active": function (callback) {chrome.tabs.query({"active": true}, function (tabs) {callback(tabs[0])})},
  "inject": function (tabId, a, b, callback) {
    chrome.tabs.executeScript(tabId, a, function () {
      var lastError = chrome.runtime.lastError;
      chrome.tabs.executeScript(tabId, b, function () {
        var lastError = chrome.runtime.lastError;
      });
    });
  }
};

app.storage = (function () {
  var objs = {};
  window.setTimeout(function () {
    chrome.storage.local.get(null, function (o) {
      objs = o;
      var script = document.createElement("script");
      script.src = "/WebAPI/common.js";
      document.body.appendChild(script);
    });
  }, 0);
  /*  */
  return {
    "read": function (id) {return objs[id]},
    "write": function (id, data) {
      var tmp = {};
      tmp[id] = data;
      objs[id] = data;
      chrome.storage.local.set(tmp, function () {});
    }
  }
})();


