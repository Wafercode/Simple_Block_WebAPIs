
chrome.webNavigation.onCommitted.addListener(function (details) {

    var WebAPI = config.options.WebAPI;
    app.tab.inject(details.tabId, {
      "allFrames": false,
      "matchAboutBlank": true,
      "runAt": "document_start",
      "frameId": details.frameId,
      "code": "var options = " + JSON.stringify(WebAPI) + ';'
    }, {
      "allFrames": false,
      "matchAboutBlank": true,
      "runAt": "document_start",
      "frameId": details.frameId,
      "file": "inject.js"
    });

});
