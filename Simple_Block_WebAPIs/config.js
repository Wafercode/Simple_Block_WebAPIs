var config = {};

config.addon = {
  set state (val) {app.storage.write("state", val)},
  set scroll (val) {app.storage.write("scroll", val)},
  get state () {return app.storage.read("state") !== undefined ? app.storage.read("state") : "ON"},
  get scroll () {return app.storage.read("scroll") !== undefined ? app.storage.read("scroll") : 0}
};

config.options = {
  "default": {   
"Beacon": true,
"BroadcastChannel": true,
"File": true,
"Fullscreen": true,
"HighPerformanceTime": true,
"History": true,
"IndexedDB": true,
"MediaRecorder": true, 
"MediaStream": true,
"MutationObserver": true,
"NavigationTiming": true,
"Notification": true,
"PointerEvent": true,
"PointerLock": true,
"Push": true,
"ResourceTiming": true,
"ServiceWorker": true,
"SpeechRecognition": true,
"WebNotifications": true,
"WebSocket": true,
"WebStorage": true,
"WebVTT": true,
"WebXRDevice": true,
"Worker": true,
"XMLSerializer": true  
  },
  set WebAPI (val) {app.storage.write("WebAPI", val)},
  get WebAPI () {return app.storage.read("WebAPI") !== undefined ? app.storage.read("WebAPI") : config.options.default}
};
